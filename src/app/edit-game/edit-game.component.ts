import { Component, OnInit } from '@angular/core';
import { GameService } from "../game.service";
import { Router } from "@angular/router";
import { Game } from "../game";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { first } from "rxjs/operators";

@Component({
  selector: 'app-edit-game',
  templateUrl: './edit-game.component.html',
  styleUrls: ['./edit-game.component.css']
})
export class EditGameComponent implements OnInit {

  game: Game;
  editForm: FormGroup;
  
  constructor(
      private formBuilder: FormBuilder,
      private router: Router, 
      private gameService: GameService
      ) {}

  ngOnInit() {
    let gameID = localStorage.getItem("editGameID");
    if(!gameID) {
      alert("Invalid action.")
      this.router.navigate(['games']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      player: ['', Validators.required]
    });
    this.gameService.getGameID(+gameID)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.gameService.updateGame(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['games']);
        },
        error => {
          alert(error);
        });
  }

}