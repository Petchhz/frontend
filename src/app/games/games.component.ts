import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Game }         from '../game';
import { GameService }  from '../game.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  games: Game[];
  isLoggedIn = false;
  myimage1 :string ="assets/img/PD1.jpg";
  myimage2 :string ="assets/img/PD2.jpg";
  myimage3 :string ="assets/img/PD3.jpg";
  myimage4 :string ="assets/img/PD4.jpg";
  myimage5 :string ="assets/img/PD5.jpg";
  myimage6 :string ="assets/img/PD6.jpg";
  myimage7 :string ="assets/img/PD7.jpg";
  myimage8 :string ="assets/img/PD8.jpg";
  myimage9 :string ="assets/img/PD9.jpg";
  myimage10:string ="assets/img/PD10.jpg";
  myimage11:string ="assets/img/PD11.jpg";
  myimage12:string ="assets/img/PD12.jpg";
  myimage13:string ="assets/img/PD13.jpg";
  myimage14:string ="assets/img/PD14.jpg";
  myimage15:string ="assets/img/PD15.jpg";
  
  constructor(
    private gameService: GameService,
    private location: Location,
    private router: Router,
    ) { }
  
  ngOnInit(): void {
    this.gameService.getGames()
    .subscribe(data => {
      this.games = data;
    });
  }

   getGames(): void {
    this.gameService.getGames()
     .subscribe(games => this.games = games);
  }  

  
  /* add(name: string ,description: string ,price: number,player: string): void {
    name = name.trim();
    if (!name) { return; }
    description = description.trim();
    if (!description) { return; }
    price = price.valueOf();
    if (!price) { return; }
    player = player.trim();
    if (!player) { return; }
    this.gameService.addGame({name} as Game)
      .subscribe(game => {
        this.games.push(game);
      });
  }  */

  delete(game: Game): void {
    this.games = this.games.filter(h => h !== game);
    this.gameService.deleteGame(game).subscribe();
  } 

  /* editGame(){
    this.router.navigate(['/edit',this.game.detail]); */

  editGame(game: Game): void{
    localStorage.removeItem("editGameID");
    localStorage.setItem("editGameID", game.id.toString());
    this.router.navigate(['edit-game']);
  }

  goBack(): void {
    this.location.back();
  }
}