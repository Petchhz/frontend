import { GameService } from './../game.service';
import { Game } from './../game';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { GamesComponent } from '../games/games.component';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {

  
  constructor(
    private gameService: GameService,
    private formBuilder: FormBuilder,
    private router: Router 
    ) {}
  
    addForm: FormGroup;
  
  
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      name: ['',Validators.required],
      description: ['',Validators.required],
      price: ['',Validators.required],
      player: ['',Validators.required]
    });
  }
  
 /*  onFileSelected(event){
    console.log(event);
  }  */
 
  SubmitAdd(gameFrom: NgForm){
    if(gameFrom.invalid){
      return;
    }
    const values = gameFrom.value;
    alert(JSON.stringify(values));
    let game = new Game();
    game.name = values.name;
    game.description = values.description;
    game.price = values.price;
    game.player = values.player;

    this.gameService.createGame(game)
    .subscribe(data=>{
      alert("Add new game complete!!");
      this.router.navigate(['games']);
    });
    error=>{
      alert(error.error.message)
    }
  }
/* onSubmit(game:Game){
    this.gameService.createGame(game)
    .subscribe(data => {
      alert("Add new game complete!!");
      this.router.navigate(['games']);
    });
  } */

  /* Show(){
    this.router.navigate(["games"])
  } */
} 

/* import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from './../game.service';
import { Game } from './../game';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-add',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css']
})
export class CreateGameComponent implements OnInit {

  constructor(private router: Router, private gameService: GameService) { }

  ngOnInit() {
  }

 SubmitAdd(gameFrom: NgForm){
   if (gameFrom.invalid){
     return;
   }
   const values = gameFrom.value;
   alert(JSON.stringify(values));
   const game = new Game();
   game.name = values.name;
   game.description = values.description;
   game.price = values.price;
   game.player = values.player;

   this.gameService.createGame(game)
   .subscribe(data => {
     alert('Successfully added');
     this.router.navigate(['games']);
   }),
   // tslint:disable-next-line: no-unused-expression
   error => {
     alert(error.error.message);
  // tslint:disable-next-line: semicolon
  }}
} */