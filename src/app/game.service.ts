import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
/* import { catchError, map, tap } from 'rxjs/operators'; */
import { Game } from './game';
import { ThrowStmt } from '@angular/compiler';


@Injectable({ 
  providedIn: 'root' 
})
export class GameService {
  
  
  /* private gamesUrl = 'http://localhost:8080/api/games'; */
  game:Game;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient ) { }

  gamesUrl = 'http://localhost:8080/api/games';

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gamesUrl);
  }

  getGameID(id: number){
    return this.http.get<Game>(this.gamesUrl + "/" + id);
  }

  /* getGameNo404<Data>(id: number): Observable<Game> {
    const url = `${this.gamesUrl}`;
    return this.http.get<Game[]>(url)
      .pipe(
        map(games => games[0]), 
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} game id=${id}`);
        }),
        catchError(this.handleError<Game>(`getGame id=${id}`))
      );
  }
 */
  /* getGame(id: number): Observable<Game> {
    const url = `${this.gamesUrl}`;
    return this.http.get<Game>(url).pipe(
      tap(_ => this.log(`fetched game id=${id}`)),
      catchError(this.handleError<Game>(`getGame id=${id}`))
    );
  } */

  /* searchGames(term: string): Observable<Game[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Game[]>(`${this.gamesUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found games matching "${term}"`) :
         this.log(`no games matching "${term}"`)),
      catchError(this.handleError<Game[]>('searchGames', []))
    );
  } */

  /* addGame (game: Game): Observable<Game> {
    return this.http.post<Game>(this.gamesUrl, game, this.httpOptions).pipe(
      tap((newGame: Game) => this.log(`added game w/ id=${newGame.id}`)),
      catchError(this.handleError<Game>('addGame')) 
    );
  } */

  deleteGame(game: Game | number): Observable<Game> {
    const id = typeof game === 'number' ? game : game.id;
    const url = `${this.gamesUrl}/${id}`;

    return this.http.delete<Game>(url/* , this.httpOptions */)/* .pipe(
      tap(_ => this.log(`deleted game topic=${id}`)),
      catchError(this.handleError<Game>('deleteGame')) */
    /* ); */
  } 
  /* createGame(game: Game):Observable<Object>{
    return this.http.post(`${this.gamesUrl}`, game);
  } */

  createGame(game: Game): Observable<Game>{
    return this.http.post<Game>(this.gamesUrl, game);
  }
  
  /* updateGame (game: Game): Observable<any> {
    return this.http.put(this.gamesUrl, game, this.httpOptions).pipe(
      tap(_ => this.log(`updated game id=${game.id}`)),
      catchError(this.handleError<any>('updateGame'))
    );
  }
 */

 updateGame(game: Game){
   return this.http.put(this.gamesUrl + '/' + game.id, game);
 }

 /*  getGamesList(): Observable<any> {
    return this.http.get(`${this.gamesUrl}`);
  }
 */
  
  /* private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  } */
  /* private log(message: string) {
    this.messageService.add(`GameService: ${message}`);
  } */
   
}
