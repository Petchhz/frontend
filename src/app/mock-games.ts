/* import { Game } from './game';

export const GAMES: Game[] = [
  { 
    id: 1, 
    name: 'Attack on Titan 2 - A.O.T.2',
    description: 'Abandon all fear. Attack on Titan 2 is the gripping sequel to the action game based on the worldwide hit anime series "Attack on Titan.',
    price: 65,
    player: 'Single',
    photoPath: 'assets/img/PD01.jpg'
  },
  {
    id: 2, 
    name: 'SCUM',
    description: 'SCUM aims to evolve the multiplayer open world survival game with unprecedented levels of character customization, control and progression, where knowledge and skills are the ultimate weapons for long-term survival.',
    price: 12,
    player: 'Online PvP',
    photoPath: 'assets/img/PD04.jpg' 
  },
  { 
    id: 3, 
    name: 'Deceit',
    description: 'Deceit tests your instincts at trust and deception in an action-filled, multiplayer first-person shooter. You wake up in unknown surroundings to the sound of the Game Master’s unfamiliar voice, surrounded by five others. A third of your group have been infected with a virus, but who will escape?',
    price: 5,
    player: 'Online PvP',
    photoPath: 'assets/img/PD07.jpg' 
  },
  { 
    id: 4, 
    name: 'PLAYERUNKNOWNS BATTLEGROUNDS',
    description: 'PLAYERUNKNOWNS BATTLEGROUNDS is a battle royale shooter that pits 100 players against each other in a struggle for survival. Gather supplies and outwit your opponents to become the last person standing.',
    price: 18,
    player: 'Online PvP',
    photoPath: 'assets/img/PD11.jpg' 
  },
  { 
    id: 5, 
    name: 'Grand Theft Auto V',
    description: 'Grand Theft Auto V for PC offers players the option to explore the award-winning world of Los Santos and Blaine County in resolutions of up to 4k and beyond, as well as the chance to experience the game running at 60 frames per second.',
    price: 24,
    player: 'Single & Online',
    photoPath: 'assets/img/PD13.jpg' 
  },
];
 */