import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing-module';

/* import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api'; */
/* import { InMemoryDataService }  from './in-memory-data.service'; */

import { RouterModule } from '@angular/router';
import { AppComponent }         from './app.component';
import { HomepageComponent }   from './homepage/homepage.component';
import { GamesComponent }      from './games/games.component';
/* import { GameSearchComponent }  from './game-search/game-search.component'; */
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { CreateGameComponent } from './create-game/create-game.component';
import { EditGameComponent } from './edit-game/edit-game.component';
import { GameService } from './game.service';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    /* RouterModule.forRoot(
      [
        { path: '',component: HomepageComponent},
        { path: 'home', component: HomepageComponent },
        { path: 'games', component: GamesComponent },
        { path: 'register', component: RegisterComponent },
        { path: 'login', component: LoginComponent },
        { path: 'add', component: CreateGameComponent },
        { path: 'edit-game', component: EditGameComponent },
      ]
    ), */
    HttpClientModule
   /*  HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ) */
  ],
  declarations: [
    AppComponent,
    RegisterComponent,
    HomepageComponent,
    LoginComponent,
    GamesComponent,
    TopBarComponent,
    CreateGameComponent,
    EditGameComponent,
    ProfileComponent
    
  ],
  providers: [ GameService ,
  authInterceptorProviders],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
