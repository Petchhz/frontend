import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class RegisterService{

    items = [];

    constructor(
        private http: HttpClient
    ){}

    addToRegister(product){
        this.items.push(product);
    }

    getItem(){
        return this.items;
    }
    
    clearRegister(){
        this.items = [];
        return this.items;
    }
}