export class Game {
    id: number;
    name: string;
    description: string;
    price: string;
    player: string; 
  }

export interface GameResponse{
    id: number;
    name: string;
    description: string;
    price: string;
    player: string;
  }

  