/* import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { RegisterService } from '../register.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit{

    items;
    checkoutForm;

    constructor(
        private registerService: RegisterService,
        private formBuilder: FormBuilder
    ) {
        this.items = this.registerService.getItem();
        this.checkoutForm = this.formBuilder.group({
            Username: '',
            Password: '',
            Nickname: '',
            Email: ''
        });
    }

    onSubmit(customerData){
        console.warn('Complete!', customerData);
        
        this.items = this.registerService.clearRegister();
        this.checkoutForm.reset();
    }

    ngOnInit(){
        this.items = this.registerService.getItem();
    }
} */

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}